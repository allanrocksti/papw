package br.unipe.fujioka.java.web;

import java.util.Date;

import javax.persistence.EntityManager;

import br.unipe.fujioka.java.web.entidades.Cliente;

public class GravaCliente {
	
	public static void main(String[] args) {
		
		EntityManagerProvider emp = new EntityManagerProvider("cadastro");
		EntityManager manager = emp.createManager();
		
		Cliente cliente = new Cliente();
		cliente.setNome("Rocks");
		cliente.setDataFim(new Date());
		cliente.setDataInicio(new Date());
		System.out.println(cliente);
		
		
		
		manager.getTransaction().begin();
		manager.persist(cliente);		
		manager.getTransaction().commit();
		
		manager.close();
		
	}
	
	
	

}
