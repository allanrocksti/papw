package br.unipe.fujioka.java.web;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.unipe.fujioka.java.web.entidades.Cliente;

public class ListaCliente {
	
	public static void main(String[] args) {
		
		EntityManagerProvider emp = new EntityManagerProvider("cadastro");
		EntityManager manager = emp.createManager();
				
		Query query = manager.createQuery("select c from Cliente c", Cliente.class);
		@SuppressWarnings("unchecked")
		List<Cliente> lista =  query.getResultList();
		
		System.out.println("#### LISTA DE CLIENTES ####");
		for(Cliente c : lista)		
			System.out.println(c.getNome());
		
		manager.close();
		
	}

}
